const express = require("express")
const app = express();
app.use(express.json())
app.use(express.urlencoded({extended : false}))

const cors = require("cors")
app.use(cors())
//const test= require("../frontend/public")

const objectid = require("objectid")

const multer = require('multer'),
uuid = require('uuid'),
DIR = './public/';;
//DIR = '../frontend/public/';;
console.log(DIR)
app.use(express.static(DIR))
/*=================================
        Database
===================================*/
const mongoose = require("mongoose")
const db_url = "mongodb+srv://anupamarmohanan:s3ABzkBLqtuCgU2a@cluster0.z9krhtn.mongodb.net/?retryWrites=true&w=majority"
const sheba_rl = "mongodb+srv://sp223he:nAQwzc4s4nGwWiZH@cluster0.pqpqgfu.mongodb.net/?retryWrites=true&w=majority"
mongoose.connect(sheba_rl,{
    useNewUrlParser: true,
    useUnifiedTopology : true,
}).then(()=>{
    console.log("Connection Successfull")
}).catch((err)=>{
    console.log(err)
})
/************schema*********** */
const userSchema = new mongoose.Schema({
    firstName : String,
    lastName : String,
    email : {
        type: String,
        // required :true,
        unique : true,
    },
    password : String,
    repassword : String,
    profileImg:String

})
const UserModel = new mongoose.model("UserModel",userSchema)

const familySchema = new mongoose.Schema({
  FamilyName: {
    type: String,
    required: true,
  },
  FamilyMembers: {
    type: [{
      type: mongoose.Schema.Types.ObjectId,
      ref: 'UserModel', // this field will refer to the User model
    }],
    default: [],
  },
  Tasks: {
    type: [{
      type: mongoose.Schema.Types.ObjectId,
      ref: 'TaskModel', // this field will refer to the Task model
    }],
    default: [],
  },
})

const FamilyModel = mongoose.model('FamilyModel', familySchema)


const taskSchema = new mongoose.Schema({
  TaskID: {
    type: Number,
    required: true,
    unique: true,
  },
  CreatedBy: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'UserModel',
    required: true,
  },
  FamilyID: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'FamilyModel',
  },
  TaskDetails: {
    type: String,
    required: true,
  },
})

const TaskModel = mongoose.model('TaskModel', taskSchema)

/*=================================
       image upload to public folder/db
===================================*/

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, DIR);
    },
    filename: (req, file, cb) => {
        const fileName = file.originalname.toLowerCase().split(' ').join('-');
        cb(null, uuid.v4() + '-' + fileName)
    }
});

var upload = multer({
    storage: storage,
    fileFilter: (req, file, cb) => {
        if (file.mimetype == "image/png" || file.mimetype == "image/jpg" || file.mimetype == "image/jpeg") {
            cb(null, true);
        } else {
            cb(null, false);
            return cb(new Error('Only .png, .jpg and .jpeg format allowed!'));
        }
    }
});


/*=================================
        get and post
===================================*/
// app.get("/",(req,res)=>{
//     res.send("App is Runing")
// })
app.post("/register",(req,res)=>{
     console.log(req.body)
    const {firstName,lastName,email,password,repassword} = req.body;
    UserModel.findOne({email: email},(err,user)=>{
            if(user){
                res.send({message : "This email id already Register"})
            }
            else{
                const user = new UserModel({
                    firstName,
                    lastName,
                    email,
                    password,
                    repassword,
                })
                user.save();
                res.send({message : "Successfull Register"})
            }
    })

})

app.post("/login",(req,res)=>{
    console.log(req.body)
    const {email,password} = req.body
    UserModel.findOne({email : email},(err,user)=>{
            if(user){
                if(password == user.password){
                    res.send({message : "Login SuccessFull",user})
                }
                else{
                    res.send({message : "Password didn't match"})
                }
            }
            else{
                res.send({message : "This email id is not register"})
            }
    })
})

app.get("/users", (req, res) => {
  UserModel.find({}, (err, users) => {
    if (err) {
      res.status(500).send({ message: "Error getting users" })
    } else {
      res.send(users)
    }
  })
})

app.post("/uploadImage", upload.single('profileImg'),async(req,res)=>{
    console.log("Inside img upload");
    const url = req.protocol + '://' + req.get('host')
    console.log(req)
    console.log(req.body,req.file)
    console.log(req.body._id)
    // let ImgPath = url + '/public/' + req.file.filename
    let ImgPath = '/public/' + req.file.filename
    const doc = await UserModel.findOne({_id:req.body._id})
    doc.updateOne({"profileImg":ImgPath}).then(result => {
       // res.send({message : "This email id is not register"})
       res.sendFile("/public/'+req.file.filename+'",{root:'.'})
    res.status(201).send({
            message: "Image uploaded!",


        })
    }
    ).catch(err => {
        console.log(err),
            res.status(500).json({
                error: err
            });
    })

})
app.get("/previewImage",(req,res)=>{
    console.log("preview")
    res.sendFile("/public/778b0390-c271-4d7e-9886-dc3fc2082f9c-image-3.jpg",{root:'.'})
})
/*===========================
TASK METHODS
===========================*/
app.post("/add_task", async (req, res) => {
  const { TaskID, CreatedBy, FamilyID, TaskDetails } = req.body;

  try {
    // Check if the user exists
    const user = await UserModel.findById(CreatedBy);

    if (!user) {
      return res.status(400).send({ message: "Invalid user" });
    }

    let family;

    // Check if the family exists, if provided
    if (FamilyID) {
      family = await FamilyModel.findById(FamilyID);

      if (!family) {
        return res.status(400).send({ message: "Invalid family" });
      }
    }

    // Create a new task object
    const task = new TaskModel({
      TaskID,
      CreatedBy,
      FamilyID,
      TaskDetails,
    });

    // Save the task object to the database
    await task.save();

    // Add the task ID to the family's Tasks array, if provided
    if (family) {
      family.Tasks.push(task._id);
      await family.save();
    }

    res.send({ message: "Task added successfully", task });
  } catch (err) {
    console.error(err);
    res.status(500).send({ message: "Server error" });
  }
})

app.post("/update_task", async (req, res) => {
  const taskId = req.body.taskId;
  const updatedTask = req.body.updatedTask;

  try {
    const task = await TaskModel.findByIdAndUpdate(
      taskId,
      updatedTask,
      { new: true }
    );

    res.status(200).json({ message: "Task updated successfully", task });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: "Server error" });
  }
})

app.get('/all_tasks', async (req, res) => {
  const createdBy = req.query.createdBy;

  try {
    const tasks = await TaskModel.find({ createdBy }).populate('CreatedBy').populate('FamilyID');

    res.status(200).json(tasks);
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'Server error' });
  }
});

app.get("/tasks_by_family", async (req, res) => {
  const familyId = req.query.familyId;

  try {
    const tasks = await TaskModel.find({ familyId });

    res.status(200).json(tasks);
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: "Server error" });
  }
})

/*=============================
FamilyID
==================================*/
app.post("/add_family", async (req, res) => {
  const { FamilyName,Creator } = req.body;

  try {
    // Create a new family object
    const family = new FamilyModel({
      FamilyName,
      FamilyMembers: [Creator],
      Tasks: [],
    });

    // Save the family object to the database
    await family.save();

    res.send({ message: "Family added successfully", family });
  } catch (err) {
    console.error(err);
    res.status(500).send({ message: "Server error" });
  }
})

app.post("/add_member", async (req, res) => {
  const { memberId, familyId } = req.body;

  try {
    // Find the family by the given familyId
    const family = await FamilyModel.findById(familyId);

    if (!family) {
      return res.status(404).send({ message: "Family not found" });
    }

    // Find the user by the given memberId
    const user = await UserModel.findById(memberId);

    if (!user) {
      return res.status(404).send({ message: "User not found" });
    }
    if (family.FamilyMembers.includes(memberId)) {
      return res.send({ message: "User is already a member of the family" });
    }

    // Add the user to the family's FamilyMembers array
    family.FamilyMembers.push(user);

    // Save the updated family object to the database
    await family.save();

    res.send({ message: "Member added successfully", family });
  } catch (err) {
    console.error(err);
    res.status(500).send({ message: "Server error" });
  }
});

/*============================
        listen
=============================*/
app.listen(8000,()=>{
    console.log("Server is runing at port 8000")
    console.log(__dirname+DIR)
})
